import { ref, computed, inject } from "vue";

export default function useTodos() {
  const service = inject("todoService") as TodoService;

  const newTodo = ref("");
  const todos = computed(() => service.getTodos());

  const addTodo = async () => {
    await service.addTodo(newTodo);

    newTodo.value = "";
  };

  const removeTodo = async (payload: any) => {
    await service.removeTodo(payload);
  };

  return {
    addTodo,
    newTodo,
    removeTodo,
    todos,
  };
}

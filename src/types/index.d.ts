declare class TodoService {
    store: any;
    getTodos(): any;
    addTodo(newTodo: any): void;
    removeTodo(payload: any): void;
}
import { useStore } from "vuex";

export class TodoService {
  store: any;

  constructor() {
    this.store = useStore();
  }

  getTodos = ():any  => {
    return this.store.state.todos;
  }

  addTodo(newTodo: any): void  {
    const value = newTodo.value && newTodo.value.trim();

    if (!value) {
      return;
    }

    const payload = {
      title: value,
      done: false,
    };

    this.store.dispatch("add", payload);
  }

  removeTodo(payload: any): void {
    this.store.dispatch("delete", payload);
  }
}


export function useTodoService(): TodoService {
  return new TodoService()
}
/*
export function useTodoService() {
  const store = useStore();

  return {
    getTodos: () => {
      return store.state.todos;
    },
    addTodo: async (newTodo: any) => {
      const value = newTodo.value && newTodo.value.trim();
      if (!value) {
        return;
      }

      const payload = {
        title: value,
        done: false,
      };

      store.dispatch("add", payload);
    },
    removeTodo: (payload: any) => {
      store.dispatch("delete", payload);
    },
  };
}

*/

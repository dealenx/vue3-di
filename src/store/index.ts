import { createStore } from "vuex";

export default createStore({
  state() {
    return {
      todos: [
        {
          done: false,
          title: "Test Title",
        },
      ],
    };
  },
  actions: {
    add: ({ commit }, payload) => {
      commit("ADD", payload);

      return payload;
    },
    delete: ({ commit, dispatch, state }, payload) => {
      commit("DELETE", payload.id);
    },
  },
  mutations: {
    ADD: (state: any, payload) => {
      state.todos.push({
        ...payload,
      });
    },
    DELETE: (state: any, id) => {
      const index = state.todos.findIndex((item: any) => item.id == id);
      state.todos.splice(index, 1);
    },
  },
});
